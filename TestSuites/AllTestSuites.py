import unittest
import os

from Package1.TC_Login_Prime import PrimeNow
from Package2.TC_Filters import Filter
from Package2.TC_Search_Bar import SearchBar

p1 = unittest.TestLoader().loadTestsFromTestCase(PrimeNow)
p2 = unittest.TestLoader().loadTestsFromTestCase(Filter)
p3 = unittest.TestLoader().loadTestsFromTestCase(SearchBar)

masterTest = unittest.TestSuite([p1])

unittest.TextTestRunner(verbosity=2).run(masterTest)